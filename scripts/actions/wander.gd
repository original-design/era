class_name WanderAction
extends GoapAction

func is_valid() -> bool:
	#return WorldState.get_elements("wood_stock").size() > 0
	return true

func get_cost(blackboard) -> int:
	#if blackboard.has("position"):
		#var closest_tree = WorldState.get_closest_element("wood_stock", blackboard)
		#return int(closest_tree.position.distance_to(blackboard.position) / 5)
	return 5

func get_preconditions() -> Dictionary:
	return {}

func get_effects() -> Dictionary:
	return {
		"is_wandering": true,
	}

var target_position = Vector2.ZERO
var last_direction = Vector2.ZERO # Store the last movement direction
var direction_bias = 0.2
# Add a variable for the chance of a complete direction change
var full_turn_chance = 0.1 # 10% chance for a full 180-degree turn

func perform(actor, delta) -> bool:
	var world_target_position = WorldState.get_state("target_position", Vector2.ZERO)
	if world_target_position != target_position:
		# If the target position from WorldState is different, update the target and calculate new direction
		target_position = world_target_position
		update_last_direction(actor.position, target_position)

	if actor.position.distance_to(target_position) > 1:
		var direction = actor.position.direction_to(target_position)
		actor.move_to(direction, delta)
	else:
		target_position = _pick_random_position(actor.position)
		WorldState.set_state("target_position", target_position)
	
	return false

func update_last_direction(current_position: Vector2, new_target_position: Vector2):
	last_direction = (new_target_position - current_position).normalized()

func _pick_random_position(current_position: Vector2) -> Vector2:
	randomize()
	
	var new_direction: Vector2
	if randf() < full_turn_chance:
		# With a certain chance, pick a completely random direction
		new_direction = Vector2(randf() * 2 - 1, randf() * 2 - 1).normalized()
	else:
		# Use last_direction as a base if it exists; otherwise, pick a random direction
		var base_direction = last_direction if last_direction.length() > 0 else Vector2(randf() * 2 - 1, randf() * 2 - 1).normalized()
		
		# Introduce a small randomness to the base_direction
		var randomness = Vector2(randf() * 2 - 1, randf() * 2 - 1).normalized()
		new_direction = base_direction.lerp(randomness, direction_bias)
	
	# Ensure new_direction is normalized and then calculate a target position based on it
	new_direction = new_direction.normalized()
	var distance = randi_range(500, 1500) # Adjust distance as needed
	var new_target_position = current_position + new_direction * distance
	
	# Update last_direction to be the new direction for next movement
	last_direction = new_direction
	
	return new_target_position

