class_name RestAction
extends GoapAction

func is_valid() -> bool:
	return true

func get_cost(blackboard) -> int:
	return 0

func get_preconditions() -> Dictionary:
	return {}

func get_effects() -> Dictionary:
	return {
		"is_resting": true,
	}

func perform(actor, delta) -> bool:
	if randf() < 0.005:
		WorldState.set_state("fatigue", 0)

	return false
