class_name Agent
extends Node

var _actor
var _goals
var _current_goal
var _current_plan
var _current_plan_step = 0
var goap

func _ready():
	goap = Goap.new()
	add_child(goap)
	print('agent is ready')

func _process(delta):
	var goal = _get_best_goal()
	
	if _current_goal == null or goal != _current_goal:
		print('goal is null')
	## You can set in the blackboard any relevant information you want to use
	## when calculating action costs and status. I'm not sure here is the best
	## place to leave it, but I kept here to keep things simple.
		var blackboard = {
			"position": _actor.position,
			}

		for s in WorldState._state:
			blackboard[s] = WorldState._state[s]

		_current_goal = goal
		print(_current_goal)
		_current_plan = goap.get_action_planner().get_plan(_current_goal, blackboard)
		print(_current_plan)
		_current_plan_step = 0
	else:
		_follow_plan(_current_plan, delta)

func init(actor, goals: Array):
	print('i am an ai agent script')
	_actor = actor
	_goals = goals


func _get_best_goal():
	var highest_priority

	for goal in _goals:
		if goal.is_valid() and (highest_priority == null or goal.priority() > highest_priority.priority()):
			highest_priority = goal

	return highest_priority

func _follow_plan(plan, delta):
	if plan.size() == 0:
		return

	var is_step_complete = plan[_current_plan_step].perform(_actor, delta)
	if is_step_complete and _current_plan_step < plan.size() - 1:
		_current_plan_step += 1
