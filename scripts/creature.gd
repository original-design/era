extends CharacterBody2D

#@onready var anim_tree = $AnimationTree
#@onready var anim_state = anim_tree.get("parameters/playback")

var ai
func _ready():
	#$CPUParticles2D.emitting = true
	ai = Agent.new()
	ai.init(self, [
		RestGoal.new(),
		WanderGoal.new()
	])
	add_child(ai)
	var input_events = get_parent().get_node("UserInput")
	input_events.connect("mouse_clicked", _on_mouse_clicked)
	#var javascript = Engine.get_singleton("JavaScript")
	#javascript.eval("alert('Hello from Godot!')")

# Assuming this script is attached to your NPC
# And your CPUParticles2D node is a child of this NPC named "OrbitParticles"

#func _process(delta):
	#var particles = $OrbitParticles
	#var distance_to_particle_center = particles.global_position.distance_to(global_position)
	#
	## Adjust gravity based on distance to simulate an attracting force
	#var gravity_strength = min(50.0 / max(distance_to_particle_center, 1), 10)  # Example calculation
	#particles.gravity = Vector2(0, -1) * gravity_strength
	#
	## Optionally, adjust particle speed to control orbit speed
	#particles.initial_velocity = max(20.0 / gravity_strength, 2)


func _on_mouse_clicked(position):
	print("Mouse clicked at: ", position)
	WorldState.set_state("target_position", position)

# Child node script
var shared_variable = ""

func set_shared_variable(value):
	shared_variable = value
	print(shared_variable)
	# You can now use shared_variable within this child node

var input_movement = Vector2.ZERO
var baseline_speed = 80
var max_speed = 160  # The speed during propulsion
var speed = baseline_speed  # Current speed starts at baseline
var deceleration = 20  # Deceleration rate
var is_propelling = false  # Flag to check if propelling

func _physics_process(delta):
	if is_propelling:
		decelerate(delta)
	elif randf() < 0.003:
		propel()

var turn_speed = 2.0
var turn_sharpness = 1.0

func move_to(target, delta):
	var direction = target.normalized()
	var target_angle = atan2(direction.y, direction.x)
	
	var max_rotation = turn_speed * turn_sharpness * delta
	var rotation_step = clamp(angle_difference(rotation, target_angle), -max_rotation, max_rotation)

	rotation += rotation_step

	var movement = Vector2(cos(rotation), sin(rotation)) * speed * delta
	move_and_collide(movement)

	update_facing()

func update_facing():
	if cos(rotation) < 0:
		scale.y = -1
	else:
		scale.y = 1
	#anim_state.travel("swimming")

func propel():
	speed = max_speed
	is_propelling = true

func decelerate(delta):
	if speed > baseline_speed:
		speed -= deceleration * delta
	else:
		speed = baseline_speed
		is_propelling = false
