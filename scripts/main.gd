extends Node2D

@onready var _fatigue_field = $Creature/StatusBox/VBoxContainer/ProgressBar

# Parent node script
var shared_variable = "i am a shared variable in the parent script"

func _ready():
	for child in get_children():
		if child.has_method("set_shared_variable"):
			child.set_shared_variable(shared_variable)

func _on_fatigue_timer_timeout():
	_fatigue_field.value = WorldState.get_state("fatigue", 0)
	if _fatigue_field.value < 100:
		_fatigue_field.value += 5

	WorldState.set_state("fatigue", _fatigue_field.value)
