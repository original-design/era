class_name ObeyGoal
extends GoapGoal

func is_valid() -> bool:
	return true

func priority() -> int:
	return 2 if WorldState.get_state("fatigue", 0) < 75 else 0

func get_desired_state() -> Dictionary:
	return {
		"is_obeying": true
	}

func _ready():
	print("testing")

#var input_events = get_parent().get_node("UserInput")
#input_events.connect("mouse_clicked", _on_mouse_clicked)
#
#func _on_mouse_clicked(position):
	#print("Mouse clicked at: ", position)
