class_name RestGoal
extends GoapGoal

func is_valid() -> bool:
	return true

func priority() -> int:
	return 1

func get_desired_state() -> Dictionary:
	return {
		"is_resting": true
	}
