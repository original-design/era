class_name WanderGoal
extends GoapGoal

func is_valid() -> bool:
	return true

func priority() -> int:
	return 2 if WorldState.get_state("fatigue", 0) < 75 else 0

func get_desired_state() -> Dictionary:
	return {
		"is_wandering": true
	}

func _ready():
	print("testing")
