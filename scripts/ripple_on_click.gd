extends Node2D  # Ensure this extends a CanvasItem-based node

signal mouse_clicked(position)

var ripple_radius = 20.0  # Radius of the ripple
var ripple_lifetime = 1.0  # How long the ripple lasts in seconds
var ripples = []  # Store ripples

func _process(delta):
	var to_remove = []
	for i in range(len(ripples)):
		ripples[i].lifetime -= delta
		if ripples[i].lifetime <= 0:
			to_remove.append(i)
	# Correctly remove items in reverse order to avoid index issues
	for i in range(len(to_remove) - 1, -1, -1):
		ripples.remove_at(to_remove[i])
	queue_redraw()

func _draw():
	for ripple in ripples:
		var alpha = ripple.lifetime / ripple_lifetime
		draw_circle(ripple.position, ripple_radius, Color(0.8, 0.8, 0.8, alpha))

func _input(event):
	# Adjusted to use the new way of calculating position
	if event is InputEventMouseButton and event.pressed:
		var mouse_pos = get_global_mouse_position()
		add_ripple(mouse_pos)
		emit_signal("mouse_clicked", mouse_pos)
	elif event is InputEventScreenTouch and event.pressed:
		var touch_pos = get_global_mouse_position()
		add_ripple(touch_pos)
		emit_signal("mouse_clicked", touch_pos)

func add_ripple(position):
	ripples.append({"position": position, "lifetime": ripple_lifetime})
	queue_redraw()
